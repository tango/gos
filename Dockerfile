###################################
#Build stage
FROM golang:1.13-alpine AS build-env

#Build deps
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache add build-base git
COPY ./ /gos
WORKDIR /gos
RUN go build -mod=vendor -ldflags="-s -w"

FROM alpine:3.10
LABEL maintainer="xiaolunwen@gmail.com"

EXPOSE 8000

VOLUME [ "/data" ]

RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk --no-cache add \
    bash \
    ca-certificates \
    curl \
    gettext \
    s6 \
    su-exec \
    tzdata

ENTRYPOINT ["/app/gos/gos"]

COPY --from=build-env ./gos/gos /app/gos/gos