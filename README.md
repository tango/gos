# Gos

Simple & Out-of-the-box Static File Web Server.

## Usage

```shell
go get gitea.com/tango/gos
go install gitea.com/tango/gos
```

**Run gos**

```shell
gos
```

**Help**

```shell
gos -help
```

```shell
Usage of gos:
  -type="file": serve file system or s3 for minio
  -dir="./public": static dir path
  -exts="": filtered ext files will be supplied
  -listDir=false: if list dir files
  -listen=":8000": listen port
  -mode=0: run mode, 0: dev, 1: product
  -pass="": basic auth user password
  -user="": basic auth user name
  -config="config.ini": default config file, default will be used if file not exist
```

## Contact

Maintain by [lunny](https://gitea.com/lunny)