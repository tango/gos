module gitea.com/tango/gos

go 1.20

require (
	code.gitea.io/log v0.0.0-20191208183219-f31613838113
	gitea.com/lunny/config v0.3.2-0.20191101015150-9ec980195544
	gitea.com/lunny/tango v0.6.3-0.20200326091825-b786e0bcf4df
	gitea.com/tango/basicauth v0.0.0-20191031125154-eb91e5efc311
	github.com/minio/minio-go/v6 v6.0.50
)

require (
	gitea.com/lunny/log v0.0.0-20190322053110-01b5df579c4e // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/minio/sha256-simd v0.1.1 // indirect
	github.com/mitchellh/go-homedir v1.1.0 // indirect
	golang.org/x/crypto v0.0.0-20190513172903-22d7a77e9e5f // indirect
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	golang.org/x/sys v0.0.0-20190429190828-d89cdac9e872 // indirect
	golang.org/x/text v0.3.0 // indirect
	gopkg.in/ini.v1 v1.42.0 // indirect
)
