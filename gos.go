// Copyright 2020 The Tango Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"fmt"
	"net/http"
	"strings"

	"gitea.com/lunny/tango"
	"gitea.com/tango/basicauth"
)

func main() {
	if err := settings(); err != nil {
		fmt.Println(err)
		return
	}

	t := tango.New()
	if *user != "" {
		t.Use(basicauth.New(*user, *pass))
		t.Logger().Info("Basic auth module loaded")
	}
	var filterExts []string
	if len(*exts) > 0 {
		filterExts = strings.Split(*exts, ",")
	}
	t.Use(tango.Logging())

	var err error
	var fs http.FileSystem
	switch *tp {
	case "dir":
		fs = http.Dir(*dir)
	case "s3":
		fs, err = s3FileSystem(*endpoint, *accessKeyID, *secretAccessKey, *bucket, *useSSL)
		if err != nil {
			fmt.Println(err)
			return
		}
	default:
		fmt.Println("Unsupported serve type")
		return
	}

	t.Use(tango.Static(tango.StaticOptions{
		ListDir:    *listDir,
		FilterExts: filterExts,
		FileSystem: fs,
	}))

	if *tls {
		t.RunTLS(*certFile, *keyFile, *listen)
	} else {
		t.Run(*listen)
	}
}
