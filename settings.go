// Copyright 2020 The Tango Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package main

import (
	"flag"
	"os"
	"os/exec"
	"path/filepath"

	"gitea.com/lunny/config"
)

var (
	tp       = flag.String("type", "dir", "serve type, could be dir or s3")
	dir      = flag.String("dir", "./public", "static dir path")
	tls      = flag.Bool("tls", false, "enable tls")
	certFile = flag.String("certFile", "", "cert file only avaible when tls is true")
	keyFile  = flag.String("keyFile", "", "key file only avaible when tls is true")
	listen   = flag.String("listen", ":8000", "listen port")
	user     = flag.String("user", "", "basic auth user name")
	pass     = flag.String("pass", "", "basic auth user password")
	listDir  = flag.Bool("listDir", false, "if list dir files")
	exts     = flag.String("exts", "", "filtered ext files will be supplied")
	cfgFile  = flag.String("config", "config.ini", "use a config file")
)

// exePath returns the executable path.
func exePath() (string, error) {
	file, err := exec.LookPath(os.Args[0])
	if err != nil {
		return "", err
	}
	return filepath.Abs(file)
}

func settings() error {
	flag.Parse()

	curExePath, _ := exePath()
	if !filepath.IsAbs(*cfgFile) {
		*cfgFile = filepath.Join(filepath.Dir(curExePath), *cfgFile)
	}

	cfg, err := config.LoadIfExist(*cfgFile)
	if err != nil {
		return err
	}

	dirCfg := cfg.MustString("dir", *dir)
	dir = &dirCfg
	listenCfg := cfg.MustString("listen", *listen)
	listen = &listenCfg
	userCfg := cfg.MustString("user", *user)
	user = &userCfg
	passCfg := cfg.MustString("pass", *pass)
	pass = &passCfg
	listDirCfg := cfg.MustBool("listDir", *listDir)
	listDir = &listDirCfg
	extsCfg := cfg.MustString("exts", *exts)
	exts = &extsCfg
	return nil
}
